:REVEAL_PROPERTIES:
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: white
#+REVEAL_EXTRA_CSS: presentation.css
#+REVEAL_TITLE_SLIDE_BACKGROUND: ./img/logo.png
#+REVEAL_TITLE_SLIDE_BACKGROUND_SIZE: 50px
#+REVEAL_TITLE_SLIDE_BACKGROUND_POSITION: top left
#+REVEAL_DEFAULT_SLIDE_BACKGROUND: ./img/logo.png
#+REVEAL_DEFAULT_SLIDE_BACKGROUND_SIZE: 50px
#+REVEAL_DEFAULT_SLIDE_BACKGROUND_POSITION: top left
#+REVEAL_TOC_SLIDE_BACKGROUND: ./img/logo.png
#+REVEAL_TOC_SLIDE_BACKGROUND_SIZE: 50px
#+REVEAL_TOC_SLIDE_BACKGROUND_POSITION: top left
#+OPTIONS: toc:1 timestamp:nil num:nil
:END:

# Azul Bolder: #003F84
# Dark: #3C3C3C
# Verde Success #00D59B
# Rosa Pink: #FF3A88
# Vermelho Warning: #FF333C
# Laranja Alert: #FF9B40
# Amarelo Attention: #FFDD24

#+TITLE: Migrando +1 Bilhão de Documentos em < 24h
#+AUTHOR: Rodrigo Fonseca
#+EXPORT_FILE_NAME: public/index.html

# https://thedevconf.com/call4papers
# https://cfp-bus.thedevconf.com.br/
# BigData and NoSQL

# Summary of your presentation, for the attendee
# Vamos falar sobre como nossa arquitetura foi evoluindo ao longo do tempo para lidar com volumes cada vez maiores de documentos até culminar com nossa primeira migração de mais de 1 bilhão de documentos e como isso se tornou uma tarefa recorrente e trivial para o time.
# Private message: tell us more about the presentation! Which references do you intend to use? How did you get interested in this area?
# Para essa apresentação vamos falar da nossa experiência prática com uma arquitetura que evoluiu de um MVP até algo que serve bilhões de documentos com baixa latência e está em constante evolução. Queremos compartilhar as dificuldades e motivações por traz de grandes migrações e a solução que criamos para resolver essas dores.

* Quem sou eu
Rodrigo Fonseca
#+REVEAL_HTML: <div class="column" style="float:left; width: 30%">
#+ATTR_HTML: :width 70% :align center
[[./img/rodrigo.png]]
#+REVEAL_HTML: </div>
#+REVEAL_HTML: <div class="column" style="float:right; width: 70%">
- Ciências da Computação pela USP em 2011
- Físico frustrado
- Staff Engineer na Arquivei
- LinkedIn: [[https://www.linkedin.com/in/rjfonseca/][rjfonseca]]
#+REVEAL_HTML: </div>
* O que é a Arquivei
** Uma plataforma para documentos Fiscais
NFe, CTe, CTeOS, MDFe, NFSe, NFCe, CFe/SAT...
[[./img/o_arquivei.gif]]
** Mais de 40 mil clientes
[[./img/clientes.png]]
** Como estamos organizados
Somos 400 pessoas!
- Engenharia
  - Plataforma DFe (⬅ você está aqui)
  - Vários times de produto
  - Times de apoio: SRE, QA, TechOps, ...
- Outras áreas: RH, Financeiro, Customer, Sales...
** Quantos documentos temos buscáveis
#+REVEAL_HTML: <div class="column" style="float:left; width: 40%">
- NFes
  - 1,9 bilhões
- CTe/CTeOS
  - 1,4 bilhões
- NFSe
  - 59 milhões
#+REVEAL_HTML: </div>
#+REVEAL_HTML: <div class="column" style="float:right; width: 60%">
#+ATTR_HTML: :width 100% :align center
[[./img/porcentagem_docs.svg]]
#+REVEAL_HTML: </div>
** Qual o tamanho da nossa infra
- ElasticSearch: 3 Clusters
  - NFes - 17 nós / 3.6 TB
  - CTe/CTeOS - 18 nós / 3.4 TB
  - NFSe - 9 nós / 104 GB
- Bigtable: 2 Instâncias
  - CTe - 3 nós / 10.2 TB
  - DFe - 4 nós / 14 TB
- BigQuery (arquivei inteira)
  - Incontáveis tabelas / 582 TB
* Evolução da arquitetura de documentos
** 👶 1ª Gen. - MVP
[[./img/evolucao_arquitetura_1.png]]
** 🏦 2ª Gen. - Multiplos bancos
[[./img/evolucao_arquitetura_2.png]]
** 🔬 3ª Gen. - Micro-serviços
[[./img/evolucao_arquitetura_3.png]]
** 🚀 4ª Gen. - Plataforma DFe
[[./img/evolucao_arquitetura_4.png]]
* Precisamos migrar 1 Bi documentos
(Da 3ª -> 4ª geração)
* Dificuldades de migrar um grande banco de dados
- Minimizar Downtime
- Garantir consistência
- Inconsistências na base original
- Auditoria
* A grande migração
** Como migrar 3ª -> 4ª geração
#+ATTR_HTML: :width 70% :align center
[[./img/passos_migracao_0.png]]
*** 1. Ingestão assíncrona
#+ATTR_HTML: :width 70% :align center
[[./img/passos_migracao_1.png]]
*** 2. Duplicar fluxo de ingestão
#+ATTR_HTML: :width 70% :align center
[[./img/passos_migracao_2.png]]
*** 3. Sincronizar o passado
#+ATTR_HTML: :width 70% :align center
[[./img/passos_migracao_3.png]]
*** 4. Chaveamento da API de busca
#+ATTR_HTML: :width 70% :align center
[[./img/passos_migracao_4.png]]
*** 5. Desligar o legado
#+ATTR_HTML: :width 70% :align center
[[./img/passos_migracao_5.png]]
** Lições aprendidas
- Planeje seus gargalos
- Passado sempre tem inconsistências
- Auditar é difícil
- Auditar é preciso
- Automação, automação e automação
* "Continuous Migration"
** Por quê?
- Troca ou teste de tecnologias
- Correção do passado
- Evolução das entidades
- Evolução da arquitetura
** Como fazemos
#+REVEAL_HTML: <div class="column" style="float:left; width: 60%">
- Orientada a eventos
- Fonte de verdade simples
- Dataflow
- Redimensionamento de clusters
- Automação
- Red/green, Feature Flags ...
- Shadowing
- Monitoria
#+REVEAL_HTML: </div>
#+REVEAL_HTML: <div class="column" style="float:right; width: 40%">
[[./img/como_fazemos.png]]
#+REVEAL_HTML: </div>
** Nossa última migração (03/2023)
- +1,8 Bi ao longo de 3 dias, ~23h30
- BigTable -> Elastic: ~ 9h30
  - Replicar BigTable: 28m
  - BigTable -> BigQuery: 5h59
  - BigQuery -> Elastic: 2h52
- Normalização do Cluster: 7h
  - Resize: 50m
  - Replicar dados: 17m
- Consumir lag: 7h
  - < 3 dias de lag / 31,3 milhões
- Payback: 9 meses
* Perguntas
** Obrigado
** Temos vagas (eventualmente)
#+ATTR_HTML: :align center
[[./img/estamos_contratando.png]]
https://arquivei.com.br/vagas
